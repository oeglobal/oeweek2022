import pytz

from django.conf import settings
import django.utils.timezone as djtz


SESSION_TIMEZONE = "django_timezone"

TIMEZONE_CHOICES = sorted(pytz.common_timezones)
TIMEZONE_ERR = "NOK"


class TimezoneMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        tzname = request.session.get(SESSION_TIMEZONE)
        if tzname and tzname != TIMEZONE_ERR:
            djtz.activate(pytz.timezone(tzname))
        else:
            djtz.deactivate()
        return self.get_response(request)


def inject_timezones(request):
    return {
        "timezone_known": SESSION_TIMEZONE in request.session,
        "timezones": TIMEZONE_CHOICES,
    }


def get_timezone(request):
    if (
        SESSION_TIMEZONE in request.session
        and request.session[SESSION_TIMEZONE] != TIMEZONE_ERR
    ):
        return request.session[SESSION_TIMEZONE]
    return settings.TIME_ZONE
